&#x26F3;  连享会课程： &ensp; [主页](https://www.lianxh.cn/news/46917f1076104.html)  || [视频](http://lianxh.duanshu.com) || [推文](https://www.lianxh.cn/news/d4d5cd7220bc7.html) || [知乎](https://www.zhihu.com/people/arlionn/) || [Bilibili 站](https://space.bilibili.com/546535876) 

&emsp;

# 经济转型理论与应用专题

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxh_emodel-01.png)](https://gitee.com/arlionn/emodel)

&emsp; 

**目录**
[[TOC]]

&emsp;

&emsp; 

## 1. 课程概况

- **主题：** 理论模型构建
- **嘉宾：** 郭凯明副教授 (中山大学)
- **模式：** 网络直播+回放
  - 全程电子板书 (课后转为 PDF 发给学员) + 课后答疑
- **时间：** 2021 年 10 月 2-3 日 (周六-周日)
  - **时段：** 上午 8:30-11:30，下午 2:30-5:30，答疑：5:30-6:00
- **课程主页：** <https://gitee.com/lianxh/emodel> &#x2B55; [PDF 课纲](https://file.lianxh.cn/KC/lianxh_emodel.pdf)   
- **报名链接：** <http://junquan18903405450.mikecrm.com/QdtTXkm>
- **助教招聘**：<https://www.wjx.top/vj/OtUnuRO.aspx>
- **打包下载：** [大纲中涉及的所有 PDF 论文](https://www.jianguoyun.com/p/DRnHCooQtKiFCBi6yYkE)

&emsp;

<div STYLE="page-break-after: always;"></div>

## 2. 课程缘起

计量方法的快速发展和数据获取的便利，使学者们得以从各个角度来刻画中国经济的发展，也借此发现了很多新现象。这些都是实证研究带给我们的福利，也是过去十多年经研究中的主流。然而，当经验证据积累到一定程度后，我们便越发迫切地想要找出这些现象背后的更一般的规律。或者说，我们不再满足于各种描述性的研究，而是更渴望能从理论层面，通过简洁、凝练的模型来刻画我们观察到的现象，以便做更深层次的思考和分析。

时至今日，得益于计算机和软件的发展，学习计量方法的成本越来越低。但是，想要学习如何建立理论模型，相关资料甚为有限。若能将 **理论和实证有机结合**，那更加难能可贵了。这是本次课程想要提供大家的。

寻找一位合适的老师来讲解这门课程并非易事。要懂理论建模；懂中国的转型特征；还要懂实证分析。只有如此，才能在讲解理论的过程中始终考虑后续的实证研究如何衔接。咨询了多位老师和学生后，推荐人选最终指向了同一个人——郭凯明老师。郭老师同时给本科生、MBA 学生和博士生讲授经济理论课程，均得到了高度认可。

2018 年，郭老师给本科生开设的《宏观经济学》课程评教成绩排名中大第一，2019 年，郭老师给博士生开设的《高级宏观经济学》评教为「5 分 (满分)」，2020 年，在针对应届本科生和研究生的问卷中，郭老师高票获评「对我影响最大的老师」，并被授予中山大学岭南学院杰出教学贡献一等奖 (每年只有一人获此殊荣)。学生们虽然觉得理论建模颇具挑战，但他们认为郭老师的《经济转型理论》是一门能大幅提升自信心的课程。

郭老师认为：教学相长——教学实际上是科研的延续，也能反哺科研。自博士期间，他一直专注于经济转型与中国经济，发表论文近 40 篇，其中《经济研究》7 篇，合著出版《[宏观经济学与中国政策](https://item.jd.com/12939704.html)》（北京大学出版社）。教学中的建模思路多源于自己的论文，因此，可以从模型设定初衷、最基本的假设条件入手，通过讨论各种可能的建模思路和弯路，让学生不自觉中已经建立起理论分析的思维模式。最终的目标是：让学生不仅能「**读懂模型**」，还能「**折腾模型**」—— 可以自己修改甚至新设模型。

&emsp;

## 3. 讲师简介

[郭凯明](https://lingnan.sysu.edu.cn/faculty/guokaiming) 老师毕业于北京大学光华管理学院，获经济学博士学位，目前为中山大学岭南学院副教授、博士生导师。他的研究领域集中在宏观经济学、发展经济学与中国经济，已经在《经济研究》（7 篇）、《管理世界》、China Economic Review、Annals of Economics and Finance 等国内外期刊发表论文近 40 篇，合著出版《[宏观经济学与中国政策](https://item.jd.com/12939704.html)》。教学方面，他曾获评中山大学岭南学院“对我影响最大的老师”，中山大学岭南学院杰出教学贡献一等奖。

郭老师非常高产，以下是他近三年的代表性成果：

1. 郭凯明，人工智能发展、产业结构转型升级与劳动收入份额变动，**管理世界**， 2019 年第 7 期。[PDF](https://file.lianxh.cn/Refs/emodel/郭凯明-2019-人工智能发展、产业结构转型升级与劳动收入份额变动.pdf)
2. 郭凯明，王藤桥，基础设施投资对产业结构转型和生产率提高的影响，**世界经济**，2019 年第 11 期。[PDF](https://file.lianxh.cn/Refs/emodel/郭凯明-2019-基础设施投资对产业结构转型和生产率提高的影响.pdf)
3. 郭凯明，杭静，颜色，资本深化、结构转型与技能溢价，**经济研究**， 2020 年第 09 期。[PDF](https://file.lianxh.cn/Refs/emodel/郭凯明-2020-资本深化、结构转型与技能溢价.pdf)
4. 郭凯明，颜色，杭静，生产要素禀赋变化对产业结构转型的影响，**经济学（季刊）**， 2020 年第 19 卷第 4 期。[PDF](https://file.lianxh.cn/Refs/emodel/郭凯明-2020-生产要素禀赋变化对产业结构转型的影响.pdf)
5. 郭凯明，潘珊，颜色，新型基础设施投资与产业结构转型升级，**中国工业经济**，2020 年第 3 期。[PDF](https://file.lianxh.cn/Refs/emodel/郭凯明-2020-新型基础设施投资与产业结构转型升级.pdf)
6. Guo, Kaiming (郭凯明), Jing Hang, and Se Yan. 2021. "Servicification of Investment and Structural Transformation: The Case of China." **China Economic Review** 67: 101621. [PDF](https://file.lianxh.cn/Refs/emodel/郭凯明-2021-Servicification-of-Investment-and-Structural-Transformation.pdf)
7. 郭凯明，颜色，李双潞，结构转型、生育率选择与人口转变，**世界经济**， 2021 年第 1 期。[PDF](https://file.lianxh.cn/Refs/emodel/郭凯明-2021-结构转型、生育率选择与人口转变.pdf)
8. 郭凯明，罗敏，有偏技术进步、产业结构转型与工资收入差距，**中国工业经济**，2021 年第 3 期。[PDF](https://file.lianxh.cn/Refs/emodel/郭凯明-2021-有偏技术进步、产业结构转型与工资收入差距.pdf)
9. 颜色，郭凯明，段雪琴，老龄化、消费结构与服务业发展，**金融研究**，2021 年第 2 期。[PDF](https://file.lianxh.cn/Refs/emodel/颜色-2021-老龄化、消费结构与服务业发展.pdf)

&emsp;

## 4. 课程详情

经济增长与经济波动是宏观经济学研究的两大问题，但经济转型对于研究中国经济问题特别重要，而且中国经济增长与波动也与经济转型密切相关。遗憾的是，传统宏观经济学课程重点在经济增长与波动，对经济转型理论的介绍甚为有限。本课程由浅入深且全面系统地介绍了经济结构转型的基本模型、主流理论与建模思想，并以中国经济为例介绍了模型量化分析方法。

课程分成「静态模型」和「动态模型」两个专题，分两天讲解。

### 第 1 讲 结构转型静态模型（1 天）    
        

本讲首先介绍结构转型模型所采用的主要模型设定和优化方法，继而以一个最基本的两部门静态一般均衡模型为例，展示结构转型的理论机制。这些建模方法和理论机制在诸多领域得到了广泛应用，包括：经济转型、经济增长、产业经济、区域经济、劳动经济等。最后，以中国农业向非农业转型的定量分析为例，展示「从理论到现实」的分析过程。

- 预备知识
  - 库恩-塔克定理
  - 消费者最优化问题
  - 企业最优化问题
- 包含结构转型的静态一般均衡模型
  - 模型框架
  - 理论分析
- 定量应用：中国农业向非农业转型

**课后阅读文献：** [查看所有](https://www.jianguoyun.com/p/DRnHCooQtKiFCBi6yYkE)

- Herrendorf, Berthold, Richard Rogerson, and Ákos Valentinyi. 2013. “Two Perspectives on Preferences and Structural Transformation.” **The American Economic Review** 103 (7): 2752–89. [PDF](https://file.lianxh.cn/Refs/emodel/Herrendorf-2013-Two-Perspectives-on-Preferences-and-Structural-Transformation.pdf)
- Duarte, Margarida, and Diego Restuccia. 2010. “The Role of the Structural Transformation in Aggregate Productivity.” **Quarterly Journal of Economics** 125 (1): 129–73. [PDF](https://file.lianxh.cn/Refs/emodel/Duarte-2010-The-Role-of-the-Structural-Transformation-in-Aggregate-Productivity.pdf)
- Cai, Wenbiao. 2015. “Structural Change Accounting with Labor Market Distortions.” **Journal of Economic Dynamics and Control** 57: 54–64. [PDF](https://file.lianxh.cn/Refs/emodel/Cai-2015-Accounting-with-Labor-Market-Distortions.pdf)
- 郭凯明，罗敏，《有偏技术进步、产业结构转型与工资收入差距》，《**中国工业经济**》，2021 年第 3 期。 [PDF](https://file.lianxh.cn/Refs/emodel/郭凯明-2021-有偏技术进步、产业结构转型与工资收入差距.pdf)
- 郭凯明，黄静萍，《劳动生产率提高、产业融合深化与生产性服务业发展》，《**财贸经济**》，2020 年第 11 期。[PDF](https://file.lianxh.cn/Refs/emodel/郭凯明-2020-劳动生产率提高-产业融合深化与生产性服务业发展.pdf)

### 第 2 讲 结构转型动态模型（1 天）      

本讲首先介绍动态模型的理论分析方法和基本概念，重点介绍不同于增长与波动模型的动态转移路径的定量模拟方法；继而介绍包括结构转型的索洛增长模型与拉姆齐模型，并将之用于中国三大产业结构转型过程的定量分析。

- **预备知识**
  - 比较静态方法
  - 跨期优化问题
  - 转移动态模拟
- **包含结构转型的索洛增长模型**
  - 模型框架
  - 理论分析
  - 定量模拟
- **包含结构转型的拉姆齐模型**
  - 模型框架
  - 动态一般均衡
- **定量应用：中国三次产业结构转型**

**课后阅读文献：** [查看所有](https://www.jianguoyun.com/p/DRnHCooQtKiFCBi6yYkE)

- Chen, Kaiji, Ayşe İmrohoroğlu, and Selahattin İmrohoroğlu. 2006. “The Japanese Saving Rate.” **The American Economic Review** 96 (5): 1850–58. [PDF](https://file.lianxh.cn/Refs/emodel/Chen-2006-The-Japanese-Saving-Rate.pdf)
- Acemoglu, Daron, and Veronica Guerrieri. 2008. “Capital Deepening and Nonbalanced Economic Growth.” **Journal of Political Economy** 116 (3): 467–98. [PDF](https://file.lianxh.cn/Refs/emodel/Acemoglu-2008-Capital-Deepening-and-Nonbalanced-Economic-Growth.pdf)
- Buera, Francisco J., and Joseph P. Kaboski. 2009. “Can Traditional Theories of Structural Change Fit the Data?” **Journal of the European Economic Association** 7: 469–77. [PDF](https://file.lianxh.cn/Refs/emodel/Buera-2009-Can-Traditional-Theories-of-Structural-Change-Fit-the-Data.pdf)
- Leukhina, Oksana M., and Stephen J. Turnovsky. 2016. “Population Size Effects in the Structural Development of England.” **American Economic Journal: Macroeconomics** 8 (3): 195–229. [PDF](https://file.lianxh.cn/Refs/emodel/Leukhina-2016-Population-Size-Effects-in-the-Structural-Development-of-England.pdf)
- 郭凯明，杭静，颜色，《资本深化、结构转型与技能溢价》，《**经济研究**》，2020 年第 9 期。[PDF](https://file.lianxh.cn/Refs/emodel/郭凯明-2020-资本深化、结构转型与技能溢价.pdf)
- 郭凯明，《人工智能发展、产业结构转型升级与劳动收入份额变动》，《**管理世界**》，2019 年第 7 期。[PDF](https://file.lianxh.cn/Refs/emodel/郭凯明-2019-人工智能发展、产业结构转型升级与劳动收入份额变动.pdf)
- Guo, Kaiming (郭凯明), Jing Hang, and Se Yan. 2021. “Servicification of Investment and Structural Transformation: The Case of China.” **China Economic Review** 67: 101621. [PDF](https://file.lianxh.cn/Refs/emodel/郭凯明-2021-Servicification-of-Investment-and-Structural-Transformation.pdf)

&emsp;

<div STYLE="page-break-after: always;"></div>

## 5. 报名信息

- **主办方：** 太原君泉教育咨询有限公司
- **标准费用**：2200 元/人 (全价)
- **优惠方案**：
  - **连享会老学员 (现场班/专题课)/团报（3 人及以上）：** 9 折，1980 元/人
  - **会员：** 8.5 折，1870 元/人
  - **温馨提示：** 以上各项优惠不能叠加使用。
- **联系方式：**
  - 邮箱：[wjx004@sina.com](wjx004@sina.com)
  - 王老师：18903405450 (微信同号)；李老师：‭18636102467 (微信同号)

>**特别提示：** 为保护讲师的知识产权和您的账户安全，系统会自动在您观看的视频中嵌入您的「**用户名**」信息，每个 **账号** 锁定一台设备进行观看，以切实保护您的权益。     

>**报名链接：** <http://junquan18903405450.mikecrm.com/QdtTXkm>     

&#x23E9; 长按/扫描二维码报名：    

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/扫码报名_转型理论.png)

### 缴费方式

> **方式 1：对公转账**    

- 户名：太原君泉教育咨询有限公司
- 账号：35117530000023891 (山西省太原市晋商银行南中环支行)
- **温馨提示：** 对公转账时，请务必提供「**汇款人姓名-单位**」信息，以便确认。

> **方式 2：扫码支付**    

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会2020暑期支付二维码180.png)

**温馨提示：** 扫码支付后，请将「**付款记录**」截屏发给王老师-18903405450（微信同号）

<div STYLE="page-break-after: always;"></div>

## 6. 招聘助教 8 名 &#x26BD;
- **任务：**
  - **A. 课前准备**：协助完成 2 篇介绍 Stata/Python/R/Matlab 和计量经济学基础知识的推文，风格类似于 [连享会主页](https://www.lianxh.cn) 推文；
  - **B. 开课前答疑**：协助学员安装课件和软件，在微信群中回答一些常见问题；
  - **C. 上课期间答疑**：针对前一天学习的内容，在微信群中答疑 (8:00-8:30，19:00-22:00)；
  - Note: 下午 5:30-6:00 的课后答疑由主讲教师负责。
- **要求：** 热心、尽职，有较好的经济学理论基础，能对常见问题进行解答和记录 (往期按期完成任务的助教优先录用，可以直接联系连老师)。
- **截止时间：** 2021 年 9 月 10 日 (将于 9 月 12 日公布遴选结果于 [课程主页](https://gitee.com/lianxh/emodel)，及 连享会主页 [lianxh.cn](https://www.lianxh.cn))

> **申请链接：** <https://www.wjx.top/vj/OtUnuRO.aspx>    

> 扫码填写助教申请资料：    

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/转型理论：助教招聘.png)

&#x26F3; **课程主页：** <https://gitee.com/lianxh/emodel>

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)

<div STYLE="page-break-after: always;"></div>

## 相关课程

### 免费公开课

- [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1 小时 40 分钟，[课程主页](https://gitee.com/arlionn/PanelData)
- [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟.
- [Stata 小白的取经之路](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) - 龙志能，时长：2 小时，[课程主页](https://gitee.com/arlionn/StataBin)
- 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles 等)

### 最新课程-直播课

| 专题                                                                   | 嘉宾   | 直播/回看视频                                                                                                                                                                                                                                                                            |
| ---------------------------------------------------------------------- | ------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| &#x2B50; **[最新专题](https://www.lianxh.cn/news/46917f1076104.html)** |        | 文本分析、机器学习、效率专题、生存分析等                                                                                                                                                                                                                                                 |
| 研究设计                                                               | 连玉君 | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B) |
| 面板模型                                                               | 连玉君 | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)                                                                                                                                 |
| 面板模型                                                               | 连玉君 | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2 小时]                                                                                                                                                                      |

- Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。

&emsp;

> #### &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom01.png)](https://www.lianxh.cn/news/46917f1076104.html)

<div STYLE="page-break-after: always;"></div>

### 关于我们     

- **Stata 连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，400+ 推文，实证分析不再抓狂。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- **公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文，获取工具软件和数据下载。常见关键词：`课程, 直播, 视频, 客服, 模型设定, 研究设计, stata, plus, 绘图, 编程, 面板, 论文重现, 可视化, RDD, DID, PSM, 合成控制法` 等

&emsp;

> 连享会小程序：扫一扫，看推文，看视频……     

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会小程序二维码180.png)](https://www.lianxh.cn/news/46917f1076104.html)

&emsp;

> 扫码加入连享会微信群，提问交流更方便      

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-学习交流微信群001-150.jpg)

&emsp; &#x270D; 连享会-常见问题解答：  
&emsp; &#x2728; <https://gitee.com/lianxh/Course/wikis>

&emsp;

> <font color=red>New！</font> **`lianxh` 命令发布了：**  
> 随时搜索连享会推文、Stata 资源，安装命令如下：  
> &emsp; `. ssc install lianxh`  
> 使用详情参见帮助文件 (有惊喜)：  
> &emsp; `. help lianxh`

&emsp;

Stata连享会 &ensp; [主页](https://www.lianxh.cn/news/46917f1076104.html)  || [视频](http://lianxh.duanshu.com) || [推文](https://www.lianxh.cn/news/d4d5cd7220bc7.html) || [知乎](https://www.zhihu.com/people/arlionn/) || [Bilibili 站](https://space.bilibili.com/546535876) 
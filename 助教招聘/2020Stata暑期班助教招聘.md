
&emsp;


「[2020-Stata暑期班](<https://gitee.com/arlionn/PX>)」（[微信版](https://mp.weixin.qq.com/s/_ypP4ol1_VnjOOBGZeAnoQ)）即将开始。为保证本次课程的全方位答疑解惑，现诚邀助教数名，主要辅助老师的教学和答疑工作。助教可以免费参加此次培训课程。

&emsp;

>## 说明和要求

**名额：** 15 名 (初级、高级和论文班各 5 名)   
**福利：** 入选助教可以免费参加对应班次的课程。  
**任务：**   
- **A. 课前准备**：协助完成 3 篇介绍 Stata 和计量经济学基础知识的文档；
- **B. 开课前答疑**：协助学员安装课件和软件，在微信群中回答一些常见问题；
- **C. 上课期间答疑**：针对前一天学习的内容，在微信群中答疑 (8:00-9:00，19:00-22:00)；
  - Note: 下午 5:30-6:00 的课后答疑由主讲教师负责。
- **要求：** 热心、尽职，熟悉 Stata 的基本语法和常用命令，能对常见问题进行解答和记录。
- **截止时间：** 2020 年 6 月 20 日 (将于 6 月 22 日公布遴选结果)

> **申请链接：** <https://www.wjx.top/jq/80875288.aspx>  
>  &emsp;   
> 亦可扫码填写助教申请资料：   
> ![连享会-2020-Stata暑期班助教招聘-扫码填写资料](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会2020暑期助教招聘二维码180.png "扫码填资料，免费听课")

> **课程主页：** <https://gitee.com/arlionn/PX>  







&emsp;


&emsp;

&emsp;


### 附：2020 Stata 暑期班课程简介
---
>  完整的知识架构是长期成长的核心动力！

&emsp;

> #### [连享会 - Stata 暑期班](https://gitee.com/arlionn/PX)     
> **线上直播 9 天**：2020.7.28-8.7  
> **主讲嘉宾**：连玉君 (中山大学) | 江艇 (中国人民大学)      
> **课程主页**：<https://gitee.com/arlionn/PX> | [微信版](https://mp.weixin.qq.com/s/_ypP4ol1_VnjOOBGZeAnoQ)  

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/2020Stata暑期班海报750.png "连享会：Stata暑期班，9天直播")

&emsp; 

## &#x2615; A. 课程概要

> **时间：** 2020 年 7 月 28 日-8 月 7 日  
> **方式：** 网络直播  
> **授课教师：** 连玉君 (初级+高级) || 江艇 (论文班)  
> **报名链接：** <http://junquan18903405450.mikecrm.com/ZXol6to>  
> **课程主页：** <https://gitee.com/arlionn/PX>  
> **Note:** 预习资料、常见问题解答等都将通过该主页发布。

> **初级班**：7 月 28-30 日 (三天), 网络直播 + 3 天回放  
> **高级班**：8 月 1-3 日 (三天), 网络直播 + 3 天回放  
> **论文班**：8 月 5-7 日 (三天), 网络直播 + 3 天回放  
> **全程班**：7 月 28 日-8 月 7 日, 网络直播 + 9 天回放

&emsp;

> **特别提示：**  
> 为保护讲师的知识产权和您的账户安全，系统会自动在您观看的视频中嵌入您的「用户名、ID 和 IP 地址」信息，每个 ID 只能锁定一台设备进行观看，以切实保护您的权益。

&emsp;



## &#x2615; B. 授课嘉宾

---
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连玉君_2020讲课照180b1.png "连玉君(中山大学)")

**[连玉君](http://lingnan.sysu.edu.cn/node/151)** ，西安交通大学经济学博士，中山大学岭南学院副教授，博士生导师。已在《China Economic Review》、《经济研究》、《管理世界》、《经济学(季刊)》、《金融研究》、《统计研究》等期刊发表论文 60 余篇。目前已完成 Panel VAR、Panel Threshold、Two-tier Stochastic Frontier 等计量模型的 Stata 实现程序，并编写过几十个小程序，如 `winsor2`, `xtbalance`, `bdiff`, `ua` 等。连玉君老师团队一直积极分享 Stata 应用中的经验，开设了 [[连享会-主页]](https://www.lianxh.cn)，[[连享会-直播间]](http://lianxh.duanshu.com)，[[连享会-知乎]](https://www.zhihu.com/people/arlionn) 等专栏，并定期在微信公众号 (**Stata连享会**) 中发布精彩推文。

---

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/江艇工作照_2020_180.png "江艇(中国人民大学)")

**江艇**，香港科技大学商学院经济学博士，中国人民大学经济学院副教授，人大国家发展与战略研究院研究员，人大微观数据与实证方法研究中心副主任，美国哥伦比亚大学商学院访问学者。主要研究领域为经济增长与发展、城市经济学、新政治经济学，在 **Economics Letters**、**Review of Development Economics**、《经济研究》、《管理世界》、《世界经济》等国内外著名学术刊物上发表多篇论文，曾应邀在多所高校讲授「应用微观计量经济学」短期前沿课程并广受好评。

&emsp;

&emsp; 

&emsp;

## 相关课程

> **连享会-直播课** 上线了！         
>  <http://lianxh.duanshu.com>  

> **免费公开课：**
> - [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟
> - [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. 
> - 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)  
> <img style="width: 170px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会短书直播间-二维码170.png">

---
### 课程一览   


> 支持回看，所有课程可以随时购买观看。

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| &#x23E9; **结构方程模型-SEM** | 阳义南 | [直播：SEM 及 Stata 应用](https://lianxh.duanshu.com/#/brief/course/6cc4caad765346cfa19c8f4f48a51ace)  <br> 2020.6.20, 88元 |
| &#x2B50; **[Stata暑期班](https://gitee.com/arlionn/PX)** | 连玉君 <br> 江艇| [线上直播 9 天](https://www.lianxh.cn/news/e87cdb5c116d0.html) <br> 2020.7.28-8.7 |
| **[效率分析-专题](https://gitee.com/arlionn/TE)** | 连玉君<br>鲁晓东<br>张&emsp;宁 | [视频-TFP-SFA-DEA](https://www.lianxh.cn/news/88426b2faeea8.html) <br> 已上线，3天 |
| **文本分析/爬虫** | 游万海<br>司继春 | [视频-文本分析与爬虫](https://www.lianxh.cn/news/88426b2faeea8.html) <br> 已上线，4天 |
| **[空间计量系列](https://lianxh.duanshu.com/#/brief/course/958fd224da8548e1ba7ff0740b536143)** | 范巧    | [空间全局模型](https://efves.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e), [空间权重矩阵](https://lianxh.duanshu.com/#/brief/course/94a5361647384a18852d28d1b9246362) <br> [空间动态面板](https://lianxh.duanshu.com/#/brief/course/f4e4b6b1e77c4ff88cecb685bbde07c3), [空间DID](https://lianxh.duanshu.com/#/brief/course/ff7dc9e0b82b40dab2047af0d01e96d0) |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)   |
|     |     | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2小时]  |


> Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。

 <img style="width: 180px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/行走的人小.jpg">



&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- **你的颈椎还好吗？** 您将 [::连享会-主页::](https://www.lianxh.cn) 和 [::连享会-知乎专栏::](https://www.zhihu.com/people/arlionn/) 收藏起来，以便随时在电脑上查看往期推文。



---

![连享会主页  lianxh.cn](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会跑起来就有风400.png "连享会主页：lianxh.cn")



--- - --

> 连享会小程序：扫一扫，看推文，看视频……

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会小程序二维码180.png)

---

> 扫码加入连享会微信群，提问交流更方便

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-学习交流微信群001-150.jpg)





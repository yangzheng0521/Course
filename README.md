
&emsp;

> Stata连享会 &ensp; [主页](https://www.lianxh.cn/news/46917f1076104.html)  || [视频](http://lianxh.duanshu.com) || [推文](https://www.lianxh.cn/news/d4d5cd7220bc7.html) || [知乎](https://www.zhihu.com/people/arlionn/) || [Bilibili 站](https://space.bilibili.com/546535876) 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-草料主页-一码平川600.png)

&emsp;


## 1. 最新课程
  
> ### &#x23E9; **2021 [理论模型构建专题](https://gitee.com/arlionn/emodel)**   
> &#x231A; 2021 年 10 月 2-3 日   
> &#x2B50; 郭凯明副教授 (中山大学)   
> &#x26EA; **课程主页**：[https://gitee.com/arlionn/emodel](https://gitee.com/arlionn/emodel)，[PDF课纲](https://file.lianxh.cn/KC/lianxh_emodel.pdf)  

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxh_emodel-01.png)](https://gitee.com/arlionn/emodel)

&emsp;

> ### &#x23C5; **2021 [课题申报专题](https://gitee.com/arlionn/kt)**   
> &#x231A; 2021 年 9 月 19-21 日  
> &#x231B; **会议形式**：讲授；一对一辅导    
> &#x2B50; 主讲：迟老师；孙老师；张老师    
> &#x26EA; **课程主页**：[https://gitee.com/lianxh/kt](https://gitee.com/arlionn/kt)，[PDF课纲](https://file.lianxh.cn/KC/lianxh_kt.pdf)    
> **报名链接**：https://www.wjx.top/vj/YIIxwmR.aspx


[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxh_kt2021_001.png)](https://gitee.com/arlionn/kt)


&emsp;



## 2. 资源分享

### 视频公开课

- [连享会码云：100多个精选计量项目](https://www.lianxh.cn/news/944a69d75cec9.html) |  [新浪视频](https://weibo.com/tv/show/1034:4479228373303338)
- [五分钟 Markdown / Markdown 幻灯片](https://gitee.com/arlionn/md) | [新浪视频](https://weibo.com/tv/show/1034:4484204327796746)
- [连老师给你的-听课建议](https://www.lianxh.cn/news/69706e871c9ad.html)
- [直击面板数据模型](http://lianxh-pc.duanshu.com/course/detail/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟，[Bilibili 站版](https://www.bilibili.com/video/BV1oU4y187qY)
- [Stata 33 讲](http://lianxh-pc.duanshu.com/course/detail/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [课程主页](https://gitee.com/arlionn/stata101)，[课件](https://gitee.com/arlionn/stata101)，[Bilibili 站版](https://space.bilibili.com/546535876/channel/detail?cid=160748)    
- [Stata小白的取经之路](https://gitee.com/arlionn/StataBin)，龙志能，上海财经大学，[去听课](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) 






### Stata

- [连享会推文](https://www.lianxh.cn) | [直播视频](http://lianxh.duanshu.com)
- **计量专题课程**: [Stata暑期班/寒假班](https://gitee.com/lianxh/PX) | [专题课程](https://gitee.com/arlionn/Course)
- Stata专栏：[最新推文](https://www.lianxh.cn) | [知乎](https://www.zhihu.com/people/arlionn/) | [CSDN](https://blog.csdn.net/arlionn) | [Bilibili](https://space.bilibili.com/546535876)
- Books and Journal: [计量Books](https://quqi.gblhgk.com/s/880197/hmpmu2ylAcvHnXwY) | [SJ-PDF](https://quqi.gblhgk.com/s/880197/eipgoUi6Gd1FDZRu) | [Stata Journal-在线浏览](https://www.lianxh.cn/news/12ffe67d8d8fb.html)
- Stata Guys：[Ben Jann](http://www.soz.unibe.ch/about_us/personen/prof_dr_jann_ben/index_eng.html) 
- &#x26F3; [连享会 · Bilibili 站](https://space.bilibili.com/546535876)

### Data
- [CSMAR-国泰安](http://www.gtarsc.com/#/datacenter/singletable) | [Wind-万德](https://www.wind.com.cn/Default.html) | [Resset-锐思](http://www.resset.cn/databases)
- [常用数据库](https://www.lianxh.cn/news/0b65fd5165c2c.html) 
- [人文社科开放数据库](https://www.lianxh.cn/news/6f06c914acde8.html) 
- [徐现祥教授-IRE-官员交流、方言等](https://www.lianxh.cn/news/8c9f81a5f19ee.html)
- [知乎-Data](https://www.zhihu.com/question/20179699/answer/681756635)

### Papers - 学术论文复现
- [论文重现网站](https://www.lianxh.cn/news/e87e5976686d5.html)
- [Google学术](https://ac.scmor.com/) | [统一入口：虫部落学术搜索](http://scholar.chongbuluo.com/) | [微软学术](https://academic.microsoft.com/home)
- [iData - 期刊论文下载](https://www.cn-ki.net/)
- [ CNKI ](http://scholar.cnki.net/) | [百度学术](http://xueshu.baidu.com/) | [Google学术](https://scholar.glgoo.org/) | [Sci-hub ](http://www.sci-hub.cc/), [2](http://sci-hub.ac/), [3](http://sci-hub.bz/), [4](http://sci-hub.ac/)
- Stata论文重现:  [Harvard dataverse][harvd] | [JFE][jfe]  | [github][git1] | [Yahoo-github][yahoogit]
- 学者主页(提供了诸多论文的原始数据和 dofiles)：[Angrist][Ang1] || [Daron Acemoglu][acem]  || [Ross Levine][ross] || [Esther Duflo][Duflo] || [Imbens](https://scholar.harvard.edu/imbens/software)  ||  [Raj Chetty](http://www.rajchetty.com/)

[harvd]:https://dataverse.harvard.edu/dataverse
[jfe]:http://jfe.rochester.edu/data.htm
[Ang1]:http://economics.mit.edu/faculty/angrist/data1/data
[acem]:http://economics.mit.edu/faculty/acemoglu/data
[ross]:http://faculty.haas.berkeley.edu/ross_levine/papers.htm
[duflo]:http://economics.mit.edu/faculty/eduflo/papers
[git1]:https://github.com/search?utf8=%E2%9C%93&q=stata&type=

[yahoogit]:https://search.yahoo.com/search;_ylt=AwrBT8di2LBZqyEAuG9XNyoA;_ylc=X1MDMjc2NjY3OQRfcgMyBGZyA3lmcC10LTQ3MwRncHJpZAMEbl9yc2x0AzAEbl9zdWdnAzAEb3JpZ2luA3NlYXJjaC55YWhvby5jb20EcG9zAzAEcHFzdHIDBHBxc3RybAMwBHFzdHJsAzE0BHF1ZXJ5A3N0YXRhJTIwZ2l0aHViBHRfc3RtcAMxNTA0NzYxODcz?p=stata+github&fr2=sb-top&fr=yfp-t-473&fp=1




&emsp;


&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)

&emsp;



&emsp;

## 相关课程

&emsp;

> **连享会-直播课** 上线了！         
>  <http://lianxh.duanshu.com>  

> **免费公开课：**    
- [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟，[课程主页](https://gitee.com/arlionn/PanelData)，[Bilibili 版](https://www.bilibili.com/video/BV1oU4y187qY)
- [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [Bilibili 版](https://space.bilibili.com/546535876/channel/detail?cid=160748)   
- [Stata 小白的取经之路](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) - 龙志能，时长：2 小时，[课程主页](https://gitee.com/arlionn/StataBin) 
- 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)   


&emsp;

---
### 课程一览   


> 支持回看

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| &#x2B50; **[最新专题](https://www.lianxh.cn/news/46917f1076104.html)**|   | 因果推断, 空间计量，寒暑假班等  |
| &#x2B55; **[数据清洗系列](https://gitee.com/arlionn/dataclean)** | 游万海| 直播, 88 元，已上线 |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)   |
| 面板模型 | 连玉君  | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2小时]  |



> Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。



&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，500+ 推文，实证分析不再抓狂；[Bilibili 站](https://space.bilibili.com/546535876) 有视频大餐。
- **公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文，获取工具软件和数据下载。常见关键词：`课程, 直播, 视频, 客服, 模型设定, 研究设计, stata, plus, 绘图, 编程, 面板, 论文重现, 可视化, RDD, DID, PSM, 合成控制法` 等



> <font color=red>New！</font> **`lianxh` 命令发布了：**    
> 随时搜索连享会推文、Stata 资源，安装命令如下：  
> &emsp; `. ssc install lianxh`  
> 使用详情参见帮助文件 (有惊喜)：   
> &emsp; `. help lianxh`


